﻿using UnityEngine;
using System.Collections;

public class ColorChange : MonoBehaviour {

	public GameObject cube;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ChangeColor(){

		Color cubeColor = new Color (Random.Range (0f, 1f), Random.Range (0f, 1f), Random.Range (0f, 1f));
		cube.transform.GetComponent<Renderer> ().material.color = cubeColor;	

	}
}
